Release Notes.1 for
•	spniCore.js
•	spniSelect.js

To view these files click on them to open and then press the EDIT button. (It appears on the same line as the name of the file.) The files will then appear formatted as they should be and the comments will be much easier to read. Please choose 'Cancel' when you are done browsing as these files are open to the public.

I am releasing these files to show some of the work I have done to date on the "spnati" program. spniSelect, which is probably the most cimplicated of the .js files, has undergone comprehensive unit testing -- spniCore not so much. Read the comments at the beginning of each file to get an idea of the design approach that I am taking with this project.

Generally, I have introduced formal object classes and tried make the program more 'object oriented'. Practically speaking, this means more direct use of data structures and less indirection. For instance, you will notice far fewer occurrences of indices in this code, indices being tantamount to pointers. Unfortunately, there are cases in which indirect references are unavoidable (see at end).

The overall design of this program was good, but as is inevitably the case (alas), the expression of it was not fully carried out. Be sure to read the file "What is RISA?" that came with this release for an explanation of what RISA stands for and how it serves in implementing "program recovery".

While an exact mapping of old code to new is unusual, it is still possible to make some comparisons between related sections of source files. If you take the trouble to do so, I am sure you will see that the new files are considerably simpler than the originals. I sincerely hope that when this project is over, the recovered files will become the new permanent basis for this application. They will always remain in the public domain.

Since the basic goal of recovery is to maintain the functionality of the original code, I have not been motivated to make any major improvements to it. However, there are several areas that I thought should be addressed.

1. When assigning a character to an opponent player (Opponent method Assign), it is necessary to remove the character from the list of assignable characters that displays on the individual character selection screen. This prevents duplication of the character in the game. If 'Remove Opponent' is selected on the opponent selection screen, the character must be restored to the assignables list.

The solution being employed was to rebuild the entire array of assignables
from the complete character inventory, a task that requires re-filtering, finding
and excluding already assigned characters, and then re-sorting the entire
assignables array, every time 'Select Opponent' was chosen on the opponent
selection screen. This seemed a little kludgy.

The present solution manages the assignables list by nulling out elements of it when characters are assigned and restoring them when a player is deassigned. This approach entails the introduction of pointers to pointers, which is rather complicated, and ends up spilling more ink on the page than I would ordinarily wish for, but it does technically work well.

2. A lot of complex code was devoted to calculating the unique dialogue lines and unique pose counts that appear when 'Credits' is selected on a character selection screen. I cut the Gordian knot here by calculating these counts for all characters at the time the character selection screens are initialized. The delay this causes the program is hardly noticeable when only eleven characters are being used, but it remains to see what happens when nearly one hundred are used. This may need to be rethought.

3. The selectors for the 'Select Opponent' buttons on the individual character selection screen are undefined in index.html. How these buttons function when pressed baffles me, since index.html defines two callbacks for the same button id.

Because the established perimeter of the spnati-recovery project encompasses only .js files, index.html cannot be changed without leaving the perimeter.


