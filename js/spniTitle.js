/* Variables and functions pertaining to the game's title and setup screens,
   clothing organization functions, and human player initialization. */

class TitleScreen extends Screen {
	constructor( ) {
		super( "#title-screen" );
	}
}

/* Tabulate all hardcoded clothing definitions; it's just easier this way.
   [Is there a 'player clothing XML file'? ClothingArticle and eClothes
   are defined in clothing.js.] */
const male_articles = [
	['Hat',			eClothes.type.extra, eClothes.position.other, 3],
	['Headphones',	eClothes.type.extra, eClothes.position.other, 3],
	['Jacket',		eClothes.type.minor, eClothes.position.upper, 3],
	['Shirt',		eClothes.type.major, eClothes.position.upper, 2],
	['TShirt',		eClothes.type.major, eClothes.position.upper, 1],	// was T-Shirt
	['Undershirt',	eClothes.type.super, eClothes.position.upper, 0],
	['Glasses',		eClothes.type.extra, eClothes.position.other, 3],
	['Belt',		eClothes.type.extra, eClothes.position.other, 4],
	['Pants',		eClothes.type.major, eClothes.position.lower, 3],
	['Shorts',		eClothes.type.major, eClothes.position.lower, 2],
	['Kilt',		eClothes.type.major, eClothes.position.lower, 1],
	['Boxers',		eClothes.type.super, eClothes.position.lower, 0],
	['Necklace',	eClothes.type.extra, eClothes.position.other, 3],
	['Tie',			eClothes.type.extra, eClothes.position.other, 3],
	['Gloves',		eClothes.type.extra, eClothes.position.other, 3],
	['Socks',		eClothes.type.minor, eClothes.position.other, 2],
	['Shoes',		eClothes.type.extra, eClothes.position.other, 3],
	['Boots',		eClothes.type.extra, eClothes.position.other, 3] ];

const female_articles = [
	['Hat',			eClothes.type.extra, eClothes.position.other, 3],
	['Headphones',	eClothes.type.extra, eClothes.position.other, 3],
	['Jacket',		eClothes.type.minor, eClothes.position.upper, 3],
	['Shirt',		eClothes.type.major, eClothes.position.upper, 2],
	['TankTop',		eClothes.type.major, eClothes.position.upper, 1],	// was Tank Top
	['Bra', 		eClothes.type.super, eClothes.position.upper, 0],
	['Glasses',		eClothes.type.extra, eClothes.position.other, 3],
	['Belt',		eClothes.type.extra, eClothes.position.other, 4],
	['Pants',		eClothes.type.major, eClothes.position.lower, 3],
	['Shorts',		eClothes.type.major, eClothes.position.lower, 2],
	['Skirt',		eClothes.type.major, eClothes.position.lower, 1],
	['Panties',		eClothes.type.super, eClothes.position.lower, 0],
	['Necklace',	eClothes.type.extra, eClothes.position.other, 3],
	['Bracelet',	eClothes.type.extra, eClothes.position.other, 3],
	['Gloves',		eClothes.type.extra, eClothes.position.other, 3],
	['Stockings',	eClothes.type.minor, eClothes.position.other, 3],
	['Socks',		eClothes.type.minor, eClothes.position.other, 2],
	['Shoes',		eClothes.type.extra, eClothes.position.other, 3] ];

var title_screen	= new TitleScreen();
var is_worn			= new Array(18); 		// initialized by game_state.Load
var clothing_cell	= '';
//0x clothingTable not in index.html

// Initialize the title screen.
function LoadTitleScreen( ) {
	LoadSavedGame();					// in save.js
	SelectCandy();
}

function SetArticleOpacity( n )  {
	$(clothing_cell + n).css( 'opacity', is_worn[n]? 1.0 : 0.3 );		// [dev]
}

/* An article of clothing on the title screen was selected.
   [rename ToggleArticle. from <div id="/male/female/clothing-container"] */
function selectClothing(n) {
	is_worn[n] = !is_worn[n];
	SetArticleOpacity( n );
}

/* A gender icon on the title screen was clicked.
   [called in save.js, also from <div id="title-gemder-block"] */
function changePlayerGender( gender ) { players[HUMAN].SetGender( gender ); }

Player.prototype.SetGender = function( gender ) {		// human only
	if (gender != this.gender) {	// we need to save and reload whenever gender changes
		this.Save();
		this.Load( gender );
	}

	const prefix = {true: "#male", false: "#female"};
	const neg = prefix[ !this.is_male ];
	const pos = prefix[  this.is_male ];

	clothing_cell = pos + "-clothing-option-";

	/* Set the clothes containers, gender buttons, and size containers on
	   the title screen. [updateTitleClothing, updateTitleGender] */
	$( neg + "-clothing-container" ).hide();
	$( neg + "-gender-button" ).css( {opacity: 0.3} );
	$( neg + "-size-container" ).hide();

	$( pos + "-clothing-container" ).show();
	$( pos + "-gender-button" ).css( {opacity: 1.0} );
	$( pos + "-size-container" ).show();

	for (n=0; n < is_worn.length; n++)		// show clothes selected to wear
		SetArticleOpacity( n );
};

/* A size icon on the title screen was selected.
   [from save.js & <div id="male/female/size-container]" */
function changePlayerSize( size ) { players[HUMAN].SetSize( size ); }

Player.prototype.SetSize = function( size ) {		// humna only
	const size_button = this.is_male? "-junk-button" : "-boobs-button";

	for (let sz in eSize)						// [eSize in Core.js]
		$( "#" + sz + size_button ).css( {opacity: (sz == size)? 1.0 : 0.3} );
	this.size = size;
};

/* For 'Enter the Inventory' on the warning screen, change to the title screen.
   [from <div id='title-start-edge' <button id='title-start-button' */
function enterTitleScreen( ) {
	ChangeScreen( warning_screen, title_screen );
}

/* For 'Enter  the Inventory' on the title screen, assign remaining properties to human.
   [from <button id="title-start-button"] */
function validateTitleScreen( ) { players[HUMAN].Finish(); }

Player.prototype.Finish = function( ) {		// human only
	// this.name and this.label are assigned in Player.Save
	this.MakeWardrobe();
//?	DisplayHumanPlayerClothing();			// in Game.js

	// Ensure the player is not wearing too much.
	if (this.clothing.length > 8) {
		$("#title-warning-label").html( "You cannot wear more than 8 articles of clothing." );
		return;
	}

	// Clear any variables pertaining to the player possibly being out of the game.
	this.out = false;
	this.folder = "opponents/human/";	// [why?]
//?	timers[HUMAN] = 0;					// in Game.js
//	gameover = false;					// in Game,js
	$player_countdown.hide();			// in Core.js

	SaveGame();
	ChangeScreen( title_screen, opponent_screen );	// and we're out
};

/* Return the human player's wardrobe. Upper and lower garments come first, then
   accessories. [This doesn't precisely preserve the original order, but it should
   still work. was WearClothing] */
Player.prototype.MakeWardrobe = function( ) {
	function BitOf( article ) {
		return (article.position == eClothes.position.other)? 0 : 1;
	}

	const folder = this.is_male? "player/male/" : "player/female/";
	this.clothing = [];						// [hope garbage collection works]

	for (n=0; n < is_worn.length; n++)
		if (is_worn[n]) {
			var article = this.is_male? male_articles[n] : female_articles[n];
			var lower_case = article[0].toLowerCase();

			this.clothing.push( new ClothingArticle( n, {
				proper:		article[0],
				lower:		lower_case,
				type:		article[1],			// importance
				position:	article[2],
				image:		folder + lower_case + ".png",	// note: lower case
				layer:		article[3]
			}) );
		}
	this.clothing.sort( (a,b) => { return BitOf(a) - BitOf(b); });
};

// Randomly select two characters as title images.
function SelectCandy( ) {
	var a = RandomInteger(1,18);		// range 1:17
	var b = a;

	while (b == a) b = RandomInteger(1,18);

	$('#left-title-candy' ).attr( 'src', 'img/candy/' + a + '-' + RandomInteger(1,5) + '.png' );
	$('#right-title-candy').attr( 'src', 'img/candy/' + b + '-' + RandomInteger(1,5) + '.png' );
}
