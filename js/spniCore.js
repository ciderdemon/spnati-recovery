/* Variables and functions that form the core of the game. Anything that is
  needed game-wide should be kept here. */

var players = [];		// array of 5 Player objects

/* The players array consists of five Player class objects. You, the human
   character, are assigned to players[0]. The other four players are subclassed
   as Opponent class. They occupy a fixed position, called a slot, on the two
   kinds of screen upon which they exclusively appear, namely, the game and the
   opponent selection screens. Because opponents are fixed to a given slot, they
   can support additional properties related to visuals.

   Objects of an entirely different class, Character, appear (also exclusively)
   on the individual and the group selection screens, categorically referred to
   as character selection screens.

   Think of players as roles to which characters are assigned before the game as
   such begins. The means of carrying out this assignment is the Opponent method
   Assign( assignable character index ). An opponent will not display anywhere
   unless it has been assigned a character.

   NEVER set any member of the players array to null. */

class Player {
	// [replaces createNewPlayer]
	constructor( p ) {						// p often symbolizes a player number
		// Constant Properties
		// These values are fixed at construction time. [Can I make them real const's?]
		this.num			= p;			// player number, range 0:4, a pointer into
											// the players array. Same as slot num if p>0.
		this.is_opponent	= (p != HUMAN);	// favor is_opponent over (this.num != HUMAN)
		this.$game_label	= $( "#game-name-label-" + p );	// [see title_screen.$name-field]
		this.$debug_button	= $( "#debug-button-" + p );

		// Variable Properties:
		this.is_assigned	= false;		// boolean: player has been assigned a character
		this.first			= "";			// string: player's first name 
		this.last			= "";			// string: player's last name 
		this.label			= "";			// string: full appellation
		this.size			= eSize.medium;	// default size of body part
		this.gender			= eGender.male;	// default player's gender (disfavored)
		this.is_male		= true;			// (favor this property over gender)
		this.hand			= new PokerHand(p);	// PlayerCards know who owns them
		this.clothing		= [];			// array of ClothingArticle; the player's attire
		this.markers		= {};
		this.timer			= 20;			// integer: time until forfeit is over
		this.forfeit		= "";			// string: state of player's forfeit 
		this.out			= false;		// boolean: player is no longer in the game
		this.exposed		= false;
		this.finished		= false;		// player is done with forfeit
		this.timeInStage	= -1;
		this.consecutive_losses = 0;
	}
} 

class Opponent extends Player {				// adds slot-related visual data
	constructor( p ) {						// player number. also slot number
		super( p );

		// Constant Properties
		// visual elements of the game screen
		this.$game_area				= $( "#game-opponent-area-" + p );
		this.$game_bubble			= $( "#game-bubble-" + p );
		this.$game_image			= $( "#game-image-" + p );
		this.$game_dialogue			= $( "#game-dialogue-" + p );
		this.$game_advance_button	= $( "#game-advance-button-" + p );

		// visual elements of the opponent selection screen
		this.$opp_button			= $( "#select-slot-button-" + p );
		this.$opp_bubble			= $( "#select-bubble-" + p );
		this.$opp_image				= $( "#select-image-" + p );
		this.$opp_dialogue			= $( "#select-dialogue-" + p );
		this.$opp_label				= $( "#select-name-label-" + p );
		this.$opp_advance_button	= $( "#select-advance-button-" + p );

		// Variable Properties
		this.anum			= -1;		// index into the assignables array or -1
		this.cnum;						// the number of the assigned character
		this.folder;					// the assigned character's folder
		this.intelligence	= eIntelligence.average;
		this.stage			= 0;
		this.current		= 0;		// integer: player's current state (state index)
		this.state			= [];		// array of OpponentState
		this.tags			= [];
		this.behavior_file	= null;		// jQuery: player's assigned behavior file
		this.GetIntelligence = this.Intelligence;	// big I
	}
}

class Screen {
	constructor( display_selector ) {
		this.$display = $(display_selector);
	}
	Show( ) { this.$display.show(); }
	Hide( ) { this.$display.hide(); }
}

// Give us show and hide methods.
class WarningScreen extends Screen {
	constructor( ) {
		super( '#warning-screen' );
	}
}

//……………………………………………… unit testing only
class PokerHand {						// defines a player’s poker hand
	constructor( p ) {					// p is the player’s number
		this.cards	= new Array(5);
		for (let card of this.cards)	// you get five aces!
			card = 0;					// [dummy]
		this.strength;
		this.value = [];				// length varies 
	}
}

class ClothingArticle {					// ex clothing.js
	constructor( n, arg ) {				// [replaces createNewClothing]
		this.proper		= arg.proper;
		this.lower		= arg.lower;
		this.type		= arg.type;
		this.position	= arg.position;
		this.image		= arg.image;
		this.layer		= arg.layer;
		this.id			= n;
	}
}

const eClothes	= {				// ex clothing.js
	type: {super: "important", major: "major", minor: "minor", extra: "extra" },
	position: {upper: "upper", lower: "lower", other: "other" }
};
//_________________________________

// General Constants
const HUMAN = 0;
const IMG = 'img/';	// directory constant
const MALE_SYMBOL = IMG + 'male.png';
const FEMALE_SYMBOL = IMG + 'female.png';
const BLANK_PLAYER_IMAGE = "opponents/blank.png";   // useful

// Enumerate player gender (disfavored), size, and intelligence level.
const eGender		= {male: "male", female: "female" };
const eSize			= {small: "small", medium: "medium", large: "large" };
const eIntelligence	= {bad: "bad", average: "average", good: "good" };

const $main_button	= $('#main-game-button');	// [ex game.js as $mainButton]
const $player_countdown = $('#player-countdown');	// [ex game.js as $gamePlayerCountdown, ref in title.js]

const $game_screen		= $('#game-screen');
const $game_table		= $('#game-table');
const $previous_screen	= null;		// screen state
const warning_screen	= new WarningScreen();

var BASE_FONT_SIZE	= 14;
var BASE_SCREEN_WIDTH = 100;
var DEBUG			= false;
var table_opacity	= 1;

//------------------------------------- ex table.js. Elim when can.
// An enumeration for stripping rules.
//var eStripRule = {loser: 'loser', winner: 'winner'};

/* The Table object maintains the state of the players and deck. It also contains
   methods for updating the visual state of the players. */
//function Table() {				// class PokerTable
//	this.deck = new Deck(); this.human = new Player(HUMAN);
//  this.ai = [];
//  this.rule = eStripRule.loser;
//}
//?var table = new Table();	// [for game screen?]
//-------------------------------------

// Load the initial game content.
function initialSetup( ) {
	players[HUMAN] = new Player(HUMAN);			// allocate object for human player
	players[HUMAN].is_assigned = true;			// you are always assigned to players[0]

	// Give human cards a special 'button' property.
//?	for (card of players[HUMAN].hand.cards)		// human.hand is already allocated
//?		card.$button = $("#player-0-card-" + card.num + 1 );	// range 1:5

	// [Opponent objects were not originally allocated here.]
	for (var p=1; p <= 4; p++)					// p is the player number
		players[p] = new Opponent(p);

	// Load content.
	table_opacity = 1;				// game_table stuff
	$game_table.css( {opacity: 1.0} );
	LoadConfigFile();
	debugger
	LoadSelectionScreens();
	LoadTitleScreen();
	AutoResizeFont();
	warning_screen.Show();			// show the warning screen
}

/* Get an opponent's property values from its assigned character's behavior file,
   which contains definitional data. The argument passed is a pointer to the
   character. [The meta.xml file duplicates some of the data in behavior.xml
   and could potentially be eliminated.
   this.cnum, this.label, and this.folder were set in Select.js by
   Opponent.Assign and Opponent.Render.] */

Opponent.prototype.LoadBehavior = function( ) {	// [ex behavior.js]
	var xml;
	var xhr = $.get( this.folder + "behaviour.xml", $noop, "text" );

	xhr.fail( (xhr, reason) => {
		alert( reason + " occurred while parsing the behavior file." );
	});
	xhr.done( () => {xml = xhr.responseText;} );	// favor .done over success:

	this.first		= $(xml).find('first').text();
	this.last		= $(xml).find('last').text();
	this.gender		= $(xml).find('gender').text().trim().toLowerCase();
	this.is_male	= (this.gender == eGender.male);
	this.size		= $(xml).find('size').text();
	this.intelligence = $(xml).find('intelligence');
	this.timer		= Number( $(xml).find('timer').text() );
	this.current	= 0;
	this.state		= [],
	this.tags		= [];
	this.behavior_file = xml;			// this is saving the !entire! file!
//	this.wardrobe	= LoadWardrobe( xml );			// [now in clothing.js]

	var t = $(xml).find('tags');

	if (t !== undefined)				// tags may not always be present
		// [Arrow functions have no overriding context.]
		$(t).find('tag').each( (n,tag) => {this.tags[n] = $(tag).text();} );
}

function LoadConfigFile( ) {
	$.ajax( "config.xml", {	method: "GET", dataType: "text",
		success:
			function( xml ) {
				DEBUG = $(xml).find('debug').text();
				console.log( "Debugging is " + DEBUG? "enabled" : "disabled" );
			}
	});
}

/* [Seems like.. the intelligence property can be dynamically altered with an array?
   of DOM elements something like <intelligence stage=n>Text Value</intelligence>.
   Does this work?] */

Player.prototype.Intelligence = function( ) {
	if (typeof(this.intelligence) === "string")
		return this.intelligence;
	else {
		var max_stage = -1;
		var best_fit = null;

		for (var element of this.intelligence) {
			var stage = parseInt( elememt.getAttribute('stage'), 10 );

			if (max_stage < stage && stage <= this.stage) {
				max_stage = stage;
				best_fit = $(element).text();
			}
		}

		if (best_fit == null)
			best_fit = eIntelligence.AVERAGE;
		return best_fit;
	}
}

// Change from one screen to another. [was screenTransition]
function ChangeScreen( prev, next ) {
	prev.Hide();
	next.Show();
}

// Reset the game state so the game can be restarted.
function ResetPlayers( ) {
	for (var player of players)
		if (player.is_assigned)
			player.InitializeStates();
}

// Restart the game.
function RestartGame( ) {
	KEYBINDINGS_ENABLED = false;
	ResetPlayers();

	// enable table opacity
	table_opacity = 1;
	$game_table.css( {opacity:1} );
	$gameplayer_clothing_area.show();
	$gameplayer_card_area.show();

	// Refresh screens
	for (var player of players)		// deassign all. [need?]
		if (player.is_opponent) player.Assign();
	UpdateAllGameVisuals();
	SelectTitleCandy();
	ForceTableVisibility( true );

	// there is only one call to this right now.
	$epilogue_selection_modal.hide();
	$game_screen.hide();
	$epilogue_screen.hide();
	warning_screen.Show();			// [dev]
}

/*** Interaction Functions ***/

/* For 'glyphicon-eye-close' (Eye-Con) on game screen, better check.
   [move to game screen. from <div id="game-screen" <div class="screen] */
//? function toggleTableVisibility

function hideSelectionTable( ) {
	ForceTableVisibility( table_opacity == 0 );
}

function ForceTableVisibility( show ) {
	if (show)
		$game_table.fadeIn();
	else
		$game_table.fadeOut();
	table_opacity = show? 1 : 0
}

/*** Utility Functions ***/

// A random integer in the range min to min-1.
function RandomInteger( min, max ) {			// [renames getRandomNumber]
	return Math.floor( Math.random() * (max - min) + min );
}

// Return the width of the visible screen in pixels. [was GetScreenWidth]
function ScreenWidth( ) {
	var screens = document.getElementsByClassName('screen');	// fetch all game screens

	// Look for a visible screen.
	for (var screen of screens)
		if (screen.offsetWidth > 0)			// screen currently visible
			return screen.offsetWidth;

	return -1;			// [added]
}

// Automatically adjust the size of font based on screen width.
function AutoResizeFont() {
	var screen_width = ScreenWidth();

	document.body.style.fontSize = (14*screen_width/1000) + 'px';
	window.onresize = AutoResizeFont;		// set up future resizing
}
