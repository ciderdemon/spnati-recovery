/* Variables and functions related to the options modal. */

const $masturbation_timer_box = $("#player-masturbation-timer-box");
const $masturbation_warning_label = $("#masturbation-warning-label");

/* For 'glyphicon-cog' on the warning/title screen, show game settings modal.
   [from <div id="warning-screen" <button id="title-settings-button"] */
function showGameSettingsModal( ) {
	$masturbation_timer_box.val( players[HUMAN].timer );	// inlines function
	$masturbation_warning_label.css( "visibility", "hidden" );
	$('#game-settings-modal').modal('show');			// ex core.js
}

/* 'Close' the game settings modal. Orig saveOptions saved this.timer and
   this.background.
   [from <div id="game-settings-modal" <div class="modal-footer"
   <button id="options-modal-button"] */
function saveOptions( ) {}

/* For 'glyphicon-cog' on the game screen, show the options modal.
   [from <div id="game-screen" <button id="game-settings-button"] */
function showOptionsModal( ) {
	$("#options-modal").modal('show');
}

/* 'Close' the options modal. Option values are now saved individually
   in this file. Elim.
   [from <div id="options-modal" <div class="modal-footer"
   <button id="options-modal-button"] */
function saveIngameOptions( ) {}

// Define auto forfeit callback.
$("#options-modal").on( "hidden.bs.modal",
	function( ) {
		if (players[HUMAN].out && AUTO_FORFEIT) {
			setTimeout( advanceGame, FORFEIT_DELAY );
			$mainButton.attr( 'disabled', true );
		}
		else
			$mainButton.attr( 'disabled', actualMainButtonState );
	}
);

/* For 'Version' button on warning/title screen, show the version modal.
   [from <div id="warning-screen" <button id="title-version-button"] */
function showVersionModal( ) {
	$('#version-modal').modal('show');
}

/* For 'Credits' button on warning/title screen, show the credits modal.
   [from <div id="warning-screen" <button id="title-credits-button"] */
function showCreditModal( ) {
	$('#credit-modal').modal('show');
}

// [Callback for 'Trophy' on title screen, loadGalleryScreen, is now in gallery.js]

// Display the active option correctly.
function SetActiveOption( selector, nlast, choice ) {
	for (var n=1; n <= nlast; n++)				// deactivate all
		$(selector + n).removeClass( "active" );
	$(selector + choice).addClass( "active" );
}

/*0x Choose table style 1 to 3.
   [setTableStyle not found and only <li id="options-table-style-1" was found.
   dev: player-table-area had no default value.] */

function setTableStyle( choice ) {
	const parts = [ 'game-table', 'game-table-surface', 'opponent-area',
					'player-table-area' ];

	for (var p of parts) {
		const $select = $('.' + p);
		var value = 'bordered ' + p;

		if (choice != 1)
			value += ' ' + p + ((choice == 2)? '-glass' : '-none');
		$select.removeClass();
		$select.addClass( value );
	}
	SetActiveOption( "#options-table-style-", 3, choice );
}

/* Change the background.
   [from <div id'game-settings-modal" <li id="settings-background-x"] */
function setBackground( choice ) {
	$("body").css( "background-image", "img/background" + choice + ".png" );
	SetActiveOption( "#settings-background-", 23, choice );
	game_state.background = choice;
}

// Change the option. [from li id="options-auto-fade-x"]
function setAutoFade( choice ) {
	AUTO_FADE = (choice != 2);

	SetActiveOption( "#options-auto-fade-", 2, choice );
	game_state.auto_fade = choice;
}

// Change the card suggestion option. [from <li id="options-card-suggest-x"]
function setCardSuggest( choice ) {
	CARD_SUGGEST = (choice == 1);

	SetActiveOption( "#options-card-suggest-", 2, choice );
	game_state.card_suggest = choice;
}

// Change the AI turn time. [from <li id="options-ai-turn-time-x"]
function setAITurnTime( choice ) {
	switch (choice) {
		case 1: GAME_DELAY = 0;		break;
		case 2: GAME_DELAY = 300;	break;
		case 3: GAME_DELAY = 600;	break;
		case 4: GAME_DELAY = 800;	break;
		case 5: GAME_DELAY = 1200;	break;
		default: GAME_DELAY = 600;
	}
	SetActiveOption( "#options-ai-turn-time-", 5, choice );
	game_state.game_delay = choice;
}

// Change the card animation speed. [from <li id="options-deal-speed-x"]
function setDealSpeed( choice ) {
	switch (choice) {
		case 1:  ANIM_DELAY = 0;   ANIM_TIME = 0;		break;
		case 2:  ANIM_DELAY = 150; ANIM_TIME = 500;		break;
		case 3:  ANIM_DELAY = 350; ANIM_TIME = 1000;	break;
		case 4:  ANIM_DELAY = 800; ANIM_TIME = 2000;	break;
		default: ANIM_DELAY = 350; ANIM_TIME = 1000;
	}
	SetActiveOption( "#options-deal-speed-", 4, choice );
	game_state.deal_animation = choice;
}

// Change forfeit option. [from <li id="options-auto-forfeit-x"]
function setAutoForfeit( choice ) {
	switch (choice) {
		case 1: FORFEIT_DELAY = 4000;	break;
		case 2: FORFEIT_DELAY = 7500;	break;
		case 3: FORFEIT_DELAY = 10000;	break;
		case 4: AUTO_FORFEIT = false;	break;
		default:AUTO_FORFEIT = true; FORFEIT_DELAY = 7500;
	}
	SetActiveOption( "#options-auto-forfeit-", 4, choice );
	game_state.auto_forfeit = choice;
}

/* Change the masturbation time.
   [from <input type="text" id="player-masturbation-timer-box"] */
function changeMasturbationTimer( ) {
	const time = Number( $masturbation_timer_box.val() );
	const is_valid = (time != "NaN" && time > 0);

	if (is_valid)
		players[HUMAN].timer = time;
	$masturbation_warning_label.css( "visibility", is_valid? "hidden" : "visible" );
}