/* Variables and functions pertaining to the gallery screen of the game. */

class GalleryScreen extends Screen {
	constructor( ) {
		super( '#gallery-screen' );

		this.page		= 0;			// [galleryPage]
		this.last_page	= 0;			// [dev. galleryPages] 
	 }
}

// [class GEending added gender, image, and is_unlocked properties to class Epilogue.]

const EPP = 20;					// epilogues per page
const $selected = {
	preview:	$('#selected-ending-previev'),		// sic
	title:		$('#selected-ending-title'),
	character:	$('#selected-ending-character'),
	gender:		$('#selected-ending-gender')
};

const $start_button	= $('#gallery-start-ending-button');	//2x
const $prev_button	= $('#gallery-prev-page-button');	//2x
const $next_button	= $('#gallery-next-page-button');	//2x
const $gender_button = $('.gallery-gender-button');		//1x
const gender_select	= '#gallery-gender-';				//1x

var $gallery_slot	= $('#gallery-endings-block').children();	//7x [$galleryEndings]
	// matches .eq[0-19] are the jQuery objects for the gallery slots
	// .children is like .find, but only descends one level]
var gallery_screen	= new GalleryScreen();		// [was $galleryScreen]
var epilogues 		= [];			// ['any and 'all' are the same]
var selected		= [];			// epilogues filtered by choice.
var chosen_ep		= -1;			// number of chosen epilogue [new]

/* For 'Trophy' button on the title screen, change screen and load all epilogues.
   [from <div id="title-screen" <button id='title-gallery-button'] */
function loadGalleryScreen( ) {
	ChangeScreen( title_screen, gallery_screen );

	LoadAllEpilogues();
	gallery_screen.SetChoice( 'all' );	// [made no sense in orig location]
	$start_button.attr( 'disabled', true );

	/* "I'm not using setInterval on purpose, although it shouldn't be necessary.
	   I don't know why, but sometimes start button enables itself at start. Strangely,
	   it only happens when I refresh Firefox after enabling it by picking a valid
	   epilogue." */
}

// [FetchLoadedEndings. isEndingLoaded can't be false]
 function LoadAllEpilogues( ) {
	var n = 0;
	for (let char of inventory)
		if (char.has_ending)
			n = GetEpiloguesFrom( char, n );

	if (epilogues.length >= 0)
		epilogues[0].is_unlocked = false;		// need a prototype for new property

	for (let ep of epilogues) {
		ep.is_unlocked = (Ep_Record( ep.folder, ep.title ) !== undefined);
		if (ep.num < 10)		            //< unit testing only >
			ep.is_unlocked = true;			//< unit testing only >
	}
}

/* [from <div id="gallery-screen" <button id="gallery-next-page-button" or
	button id="gallery-prev-page-button"] */
function galleryNextPage( ) { gallery_screen.ChangePage(  1 ); }
function galleryPrevPage( ) { gallery_screen.ChangePage( -1 ); }

GalleryScreen.prototype.ChangePage = function( step ) {
	this.page += step;
	this.RenderPage();
	$gallery_slot( 'opacity', '' );
	$prev_button.attr( 'disabled', this.page === 0 );
	$next_button.attr( 'disabled', this.page == this.last_page );
};

// [from <div id="gallery-screen" <button id="gallery-main-back-button"]
function backGalleryScreen( ) {
	ChangeScreen( gallery_screen, title_screen );
}

Epilogue.prototype.RenderSlot = function( $slot ) {
	$slot.removeClass( 'empty-thumbnail' );

	if (this.is_unlocked) {
		$slot.removeClass( 'unlocked-thumbnail' );
		$slot.css( 'background-image', 'url("' + this.screens[0].image + '")' );
	}
	else
		$slot.addClass( 'unlocked-thumbnail' );
};

// Render thumbnails on current gallery page. [was loadThumbnails]
GalleryScreen.prototype.RenderPage = function( ) {
	var k = EPP * this.page;

	for (var n=0; n < EPP; n++, k++) {
		var $slot = $gallery_slot.eq(n);

		if (k < selected.length)
			selected[k].RenderSlot( $slot );
		else {							// Show the rest of the page as empty.
			$slot.removeClass( 'unlocked-thumbnail' );
			$slot.addClass( 'empty-thumbnail' );
			$slot.css( 'background-image', 'none' );
		}
	}
};

/* Set the 'gender' button and filter the epilogues. There are four choices:
   male, female, any, and all, but any is the same as all that I can tell.
   [from <div id="gallery-screen" <div id="gallery-gender-block"] */
function galleryGender( choice ) { gallery_screen.SetChoice( choice ); }

GalleryScreen.prototype.SetChoice = function( choice ) {
	if (choice == 'any') choice = 'all';

	selected = (choice == 'all')? epilogues :
		epilogues.filter( ep => (ep.gender == choice) );

	this.page = 0;
	this.last_page = Math.ceil( selected.length / EPP ) - 1;

	$gender_button.css( 'opacity', 0.4 );			// adjust gender buttons
	$(gender_select + choice).css( 'opacity', 1 );

	$prev_button.attr( 'disabled', true );			// page is 0
	$next_button.attr( 'disabled', this.last_page === 0 );
	this.RenderPage();
};

/* For selected gallery screen icon, render graphics.
   [from <div id="gallery-screen" <div id="gallery-endings-block"] */
function selectEnding( nslot ) {
	$gallery_slot.css( 'opacity', '' );
	$gallery_slot.eq(nslot).css( 'opacity', 1 );
	selected[ gallery_screen.page*EPP + nslot ].Select();
}

Epilogue.prototype.Select = function( ) {
	const style = {true: 'male-style', false: 'female-style'};
	const is_male = (this.gender == 'male');	// !is_male isn't same as female

	$start_button.attr( 'disabled', !this.is_unlocked );
	this.RenderSlot( $selected.preview );

	chosen_ep = this.is_unlocked? this.num : -1;

	$selected.title.html( this.is_unlocked? this.title : '' );
	$selected.character.html( this.label );
	$selected.gender.html( this.gender );
	$selected.gender.removeClass( style[!is_male] );

	switch (this.gender) {
		case 'male':
		case 'female':
			$selected.gender.addClass( style[is_male] ); break;
		default:
			$selected.gender.removeClass( style[is_male] );
	}
};

/* For 'Start' button, initialize and play the epilogue.
   [from <div id="gallery-screen" <button id="gallery-start-ending-button"
   (player) $name_field declared in save.js] */
function doEpilogueFromGallery() {
    selected[chosen_ep].Start( false );
}
