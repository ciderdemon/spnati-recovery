// Load and save certain human player and game option values using cookis.js.

const $name_field = $("#player-name-field");

var game_state = {
	gender:		'male',
	true: {		// for male
		name:		'Jack',
		size:		'medium',
		clothing:	// [2,4,7,8,11,14,15,17]
			[false, false, true, false, true, false, false, true, true,
			false, false, true, false, false, true, true, false, true]
		// male attire: Jacket, TShirt, Belt, Pants, Boxers, Gloves, Socks, Boots
	},
	false: {		// for female
		name:		'Jill',
		size:		'medium',
		clothing:	// [2,4,5,7,8,11,15,17]
			[false, false, true, false, true, true, false, true, true,
			false, false, true, false, false, false, true, false, true]
		// female attire: Jacket, TankTop, Bra, Belt, Pants, Panties, Stockings, Shoes
	},
	timer:			20,
	background:		1,		// background number
	auto_fade:		1,		// auto-fade option
	card_suggest:	2,		// card suggestion option
	game_delay:		3,		// AI turn time
	deal_animation:	3,		// card animation (deal) speed
	auto_forfeit:	4,		// auto forfeit off
	unlocked_ep:	[]		// unlocked episode records
};

/* Load stored values.
   [UpgradeCookie(cookie); May need new converter to convert the old save
   format to the current one.] */
function LoadSavedGame( ) {				// [was LoadCookie]
	var cookie = Cookies.get( 'game_state' );		// returns a Promise

	if (cookie !== undefined)
		game_state = JSON.parse(cookie);	// else use default
	LoadGame.call( game_state );
}

function LoadGame( ) {
	// Initialize human player.
	players[HUMAN].Load( this.gender );
	setBackground( this.background );

	// Initialize options. [inline LoadOptions]
	setAutoFade( this.auto_fade );
	setCardSuggest( this.card_suggest );
	setAITurnTime( this.game_delay );
	setDealSpeed( this.deal_animation );
	setAutoForfeit( this.auto_forfeit );
}

// Save options and human player values.
function SaveGame( ) {
	players[HUMAN].Save();
	// 'ingame options' are now saved individually in Options.js
	// unlocked_ep epilogues are saved by UnlockEnding

	Cookies.set( 'game_state', game_state, {expires: 3652} );
}

Player.prototype.Load = function( gender ) {
	this.gender		= gender;
	this.is_male	= (gender == eGender.male);
	this.SetGender( gender );

	var human = game_state[ this.is_male ];
	this.first = human.name;
	$name_field.val( human.name );
	this.SetSize( human.size );
	is_worn = human.clothing;
	this.timer = game_state.timer;
}

Player.prototype.Save = function( ) {
	this.first = $name_field.val();
	if (this.first == "")
		this.first = this.is_male? "Jack" : "Jill";
	this.label = this.first;
	this.$game_label.html( this.first );	// in Game.js

	game_state[this.is_male] = {
		name:		this.first,
		size:		this.size,
		clothing:	is_worn
	};
	game_state.gender	= this.gender;
	game_state.timer	= this.timer;		// set in Options.js
}

// Find unlocked epilogue record. Return undefined if not found.
function Ep_Record( folder, title ) {
	let reccrd = {folder: folder, title: title};
	return game_state.unlocked_ep.find( ep =>
		{return ep == record;} );	// return 1st match
}

// [from Epilogue.Initialize]
function UnlockEnding( folder, title ) {			// [was AddEnding]
	var record = Ep_Record( folder, title );

	if (record == undefined) {			// no match
		game_state.unlocked_ep.push( record );
		game_state.unlocked_ep.sort( Compare_Ep_Records );
	}
	SaveGame();
}

function Compare_Ep_Records( a, b ) {
	var compare;
	var v1, v2;

	// If the names are the same, compare the titles.
	if (a.folder == b.folder) {
		v1 = a.title;
		v2 = b.title;
	}
	else {
		v1 = a.name;
		v2 = b.name;
	}

	if (v1 < v2)
		compare = -1;
	else if (v1 > v2)
		compare = 1;
	else
		compare = 0;
	return compare;	 	// sort direction
} 

// 0x
function CharNameFrom( folder ) {		// [was getFolderName]
	var name = folder;
	return name.substr(10);		// skip over "opponents/"

	//?	if (name.startsWith('/')) name = name.substr(1);
	//?	if (name.endsWith('/')) name = name.substr( 0, name.length-1 );
}

function MergeObjects( a, b ) {			// [keep this around. is recursive.]
	if (b == undefined)
		return a;
	else if (a == undefined)
		return b;

	for (let v in b)
		if (typeof a[v] == 'object')
			a[v] = mergeObjects( a[v], b[v] );
		else
			a[v] = b[v];
	return a;
}


