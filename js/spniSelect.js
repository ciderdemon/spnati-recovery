/* Variables and functions related to the game's three selection screens. */

/* It is a good idea to distinguish potential opponents from real ones. The
   class Character identifies potential opponents who can become real ones by
   being assigned to an Opponent class player. Characters appear exclusively
   on character selection screens (either as individuals or in a group),
   whereas Opponent class players appear exclusively on the game and the
   opponent selection screens. (Also see notes for Player class.)

   This class distinction has not been exported to the outside world as of yet.
   There opponent continues to be synonymous with character.

   Character class objects are constructed from data found in files having names
   of the form: 'opponent'/meta.xml, where 'opponent' is the text of an XML
   element tagged as <opponent> within the file opponents/listing.xml. *whew* */

class Character {
	/* Create a character from the defining data in its folder. This only works
	   because the success callback is defined as an arrow function, which has no
	   context of its own to hide the constructor's.
	   [See Opponent.LoadListingFile and elsewhere for different approaches to I/O.
	   'release' did not appear in loadGroupMember.] */
	constructor( n, folder ) {
		this.num	= n;				// the character number, an inventory index
		this.folder = folder;

		var xhr = $.get( folder + "meta.xml", xml => {	// xml is the file. a DOM object
			this.first		= $(xml).find('first').text();
			this.last		= $(xml).find('last').text();
			this.label		= $(xml).find('label').text();
			this.gender		= $(xml).find('gender').text();
			this.source		= $(xml).find('from').text();
			this.image		= $(xml).find('pic').text();
			this.layers		= $(xml).find('layers').text();
			this.height		= $(xml).find('height').text();
			this.artist		= $(xml).find('artist').text();
			this.writer		= $(xml).find('writer').text();
			this.description= $(xml).find('description').text();
			this.is_enabled	= ($(xml).find('enabled').text() == "true");	// [was enabled]
			this.has_ending	= ($(xml).find('has_ending').text() == "true");	// [was ending]
			this.release	= parseInt( $(xml).find('release').text() );
			this.unique_lines = 0;			// [was uniqueLineCount]
			this.num_images	= 0;			// [was posesImageCount]
		}, "text" );

		xhr.fail( function( xhr, reason ) {
			console.log( reason + " occurred while creating a character from " +
				folder + "meta.xml" );
		});
	}
}

// These objects can only be constructed after the inventory has been completed.
class CharacterGroup {
	constructor( $grp ) {					// [renames createNewGroup]
		var found = false;

		this.is_enabled	= true;				// [dev: new. Always true at present.]
		this.title		= $grp.attr('title');
		this.members	= new Array(4);		// array of character numbers (cnum's)

		for (let slot=1; slot <= 4; slot++) {
			var folder = $grp.attr( 'opp'+slot );

			// Find character by matching folder names and store its number.
			for (let char of inventory) {
				found = (char.folder == folder);
				if (found) {
					this.members[slot-1] = char.num;
					break;				// really clumsy
				}
			}

			if (!found)
				alert( "Undefined character in " + this.title + ", " + folder );
		}
	}
}

/* Players that have had characters assigned to them appear on the opponent
   selection screen. Slot-dependent graphical elements are defined mostly in
   Opponent.Render. Formerly called the main selection screen. */

class OpponentScreen extends Screen {		// abbreviates OpponentSelectionScreen
	constructor( ) {
		super( "#main-select-screen" );

		// constant properties
		this.$table				= $("#select-table");
		this.$start_button		= $("#main-select-button");
		this.$random_buttons	= [ $("#select-random-button"),
									$("#select-random-female-button"),
									$("#select-random-male-button") ];
		this.$remove_all_button = $("#select-remove-all-button");

		// variable properties
		this.table_is_hidden = false;
	}
}

class CharacterScreen extends Screen {		// abbreviates CharacterSelectionScreen
	// LoadListingFile must run before these objects can be constructed.
	constructor( screen_type ) {			// slot-dependent data are not included
		const prefix = prefix_of[screen_type];
		super( prefix + "-select-screen" );

		// const properties
		this.is_single	= (screen_type == eScreen.single);
		this.prefix		= prefix;
		this.$table		= $(prefix + "-select-table");		// “Dock Area”
		this.stats		= prefix + "-stats-page-";

		// variable properties
		this.page_num	= 0;
		this.number_pages = this.is_single?
			Math.ceil(assignables.length/4) :
 			character_groups.length;
		this.table_is_hidden = false;
	}

	SetPageNumber( ) {
		$(this.prefix + "-page-indicator").val( this.page_num + 1 );
	}
	SetMaximumPage( ) {
		$(this.prefix + "-max-page-indicator").html( "of " + this.number_pages );
	}
}

/* Opponent Selection Screen Related Data */

var opponent_screen = new OpponentScreen();		// [was $selectScreen]

/* Character Selection Screen Related Data */

const eScreen = {			// enumerates character screen types
	single: "single",		// individual (alias single) selection screen
	group:  "group"			// group selection screen
};
const name_of   = { single: "individual", group: "group" };
const prefix_of = { single: "#individual", group: "#group" };

var character_screen = {single: null, group: null};
							// [was $/individual/group/SelectScreen]
var inventory		= [];	// immutable array of all characters in listing order. [I
							// prefer this name to 'characters'; was loadedOpponents]
var assignables		= [];	// mutable array of characters [was selectableOpponents]
var assigneds		= [];	// indices of assigned characters in ascending order
var character_groups= [];	// array of all character groups [was loadedGroups]
var chosen_gender	= -1; 	// [refs in changeSearchGender and closeSearchModal]
//[0x hiddenOpponents individualSlot individual/group/CreditsShown shown/Individuals/Group/]

/* Search and Sort Variables */

const $search_modal		= $('#search-modal');
const $search_name		= $("#search-name");
const $search_source	= $("#search-source");
const $search_gender_options = [$("#search-gender-1"), $("#search-gender-2"),
								$("#search-gender-3")];
const sort_by = {
	Newest:				["-release"],
	Oldest:				["release"],
	"Most Layers":		["-layers"],
	"Fewest Layers":	["layers"],
	Multi:				["release", "layers"] };	// multi-field example
var sort_option = "Featured";

/****** Start Up Functions ******/

// Load all of the content required to display the screen.
function LoadSelectionScreens( ) {
	LoadListingFile();

	for (var char of inventory)	// better get counts here than in Character constructor
		char.GetCounts();		// uses behavior.xml

	for (var type in eScreen) {		// must follow LoadListingFile
		character_screen[type] = new CharacterScreen( type );
		character_screen[type].SetPageNumber();
		character_screen[type].SetMaximumPage();
	}
	character_screen.single.target = undefined;		// new property. see SelectOpponent

	for (var player of players)			// close the bubbles and we're ready to go
		if (player.is_opponent) player.$opp_bubble.hide();
}

/* Load the characters and groups declared in the listing file. Listing file
   order is preserved. [was loadListingFile; absorbs loadOpponentMeta loadGroupMeta
   loadGroupMemberMeta] */

function LoadListingFile( ) {
	$.ajax( "opponents/listing.xml", {method: "GET", dataType: "text",
		success: listing => {
			// Create and enlist characters from data in their folders.
			$(listing).find('individuals').find('opponent').each(
				function(cnum) {				// 'this' is an <opponent element
					inventory[cnum] = new Character( cnum, $(this).text() );
					assignables[cnum] = cnum;
				}
			);

			// Create new character groupings and enlist them. Absolutely
			// must follow construction of the character inventory.
			$(listing).find('groups').find('group').each(
				function(cnum) {		 		// 'this' is a <group element
					character_groups[cnum] = new CharacterGroup( $(this) );
				}
			);
		},
		error: function(xhr, reason) {
			alert( reason + " occurred while parsing the listing file" );
		}
	});
}

/* Calculate and return the real assignables index of a displayed character
   from its position on the character screen. Fast algorithm effectively
   partially merges the displayed indices with the assigned indices. */

function AssignablesNumOf( pos ) {		// or AnumOf
	for (var n=0; n < assigneds.length && assigneds[n] <= pos+n; n++);
	return pos+n;						// real anum

// [0,1,2,3,4,5,6,7,..]			assign display:2, real: 2
// [0,1,n,3,4,5,6,7,..]	+ [2]	assignables + assigneds 
// [0,1,2,3,4,5,6,7,..]			assign display:2, real:3
// [0,1,n,n,4,5,6,7,..]	+ [2,3]
// [0,1,2,3,4,5,6,7,..]			assign display:4, real:6
// [0,1,n,n,4,5,n,7,..]	+ [2,3,6]
// [0,1,2,3,4,5,6,7,..]			assign display:1, real:1
// [0,n,n,n,4,5,n,7,..]	+ [1,2,3,6]
}

// Look up cnum in the assignables array.
function AnumOf( cnum ) {			// [JS can't do overloading.]
	return assignables.findIndex( c => {return c == cnum;} );
}

/****** begin Character Selection Screen Related Functions ******/

/* Write the graphical values associated with a given character selection screen
   slot to index.html. Arg2 is a pointer to the slot's occupant. What eventually
   appears on the screen depends upon which '-stats-page-' is showing.
   [Does not use height or release. No "-height-label-"] */

CharacterScreen.prototype.RenderSlot = function( slot, cnum ) {
	const prefix = this.prefix;

	function $Select( property ) {				// [alters context of 'this']
		return $( prefix + property + slot );	// return a jQuery object
	}

	if (cnum != null) {					// [this is why this can't be a Character method]
		const char = inventory[cnum];

		// Basic Info [-stats-page-x-1]
		$Select("-name-label-"		).html( char.first + " " + char.last );
		$Select("-sex-label-"		).html( char.gender );
		$Select("-source-label-"	).html( char.source );

		// Credits [-stats-page-x-2]
		$Select("-writer-label-"	).html( wordWrapHtml(char.writer) );
		$Select("-artist-label-"	).html( wordWrapHtml(char.artist) );
		$Select("-counts-"			).css( "visibility", "visible" );
		$Select("-line-count-label-").html( char.unique_lines );
		$Select("-pose-count-label-").html( char.num_images );

		// More Info [-stats-page-x-3]
		$Select("-description-label-").html( char.description );

		// Always [class=“image-row”]
		$Select("-image-"			).attr( 'src', char.folder + char.image );
		$Select("-layer-"			).show();			// [need]
		$Select("-layer-"			).attr( 'src', "opponents/layers"+char.layers+".png" );
		char.has_ending? $Select("-badge-").show() : $Select("-badge-").hide();

		// Not Found in index.html
		$Select("-prefers-label-"	).html( char.label );

/*		[Selector not in index.html. It reuses <button id="select-slot-button-x".
		Somehow the callback works, but this doesn't.]
		if (this.is_single) {	// single selection buttons only exist on single screen
			$Select("-button-").html( char.is_enabled? "Select Opponent" : "Coming Soon" );
			$Select("-button-").attr( "disabled", !char.is_enabled );
		} */
	}
	else { // Clear the slot. [should be a master disable for all this]
		$Select("-name-label-"		).html("");
		$Select("-sex-label-"		).html("");
		$Select("-source-label-"	).html("");

		$Select("-writer-label-"	).html("");
		$Select("-artist-label-"	).html("");
		$Select("-counts-"			).css( "visibility", "hidden" );
			// this hides "-line-count-label-" and "-pose-count-label-"
		$Select("-description-label-").html("");

		$Select("-image-"			).attr( 'src', BLANK_PLAYER_IMAGE );
		$Select("-badge-"			).hide();
		$Select("-layer-"			).hide();
		$Select("-prefers-label-"	).html("");

/*		if (this.is_single)
			$Select("-button-").attr( "disabled", true ); */
	}
}

/* Write the graphical values of a four character selection screen 'page' to
   index.html. (The screen will not display until its Show method is called.)

   [It is possible to eliminate the null cnum's and work entirely with the
   assigneds array, since a reference to null cnum appears here only, but the	
   added complexity does not seem worth it.
   No longer skips empty pages. Replaces update/Individual/Group/SelectScreen.] */

CharacterScreen.prototype.RenderPage = function( ) {
	const npage = this.page_num;
	var slot = 1;

	if (this.is_single) {
		if (assignables.every( cnum => {return cnum == null;} ))
			clearSearch();				// no assignables are left. auto-rebuild.

		// Fill the slots with assignable characters.
		for (var anum = AssignablesNumOf(4*npage); anum < assignables.length &&
			slot <= 4; anum++)
			if (assignables[anum] != null) {
				this.RenderSlot( slot, assignables[anum] );
				slot++;
			}

		for( ; slot <= 4; slot++)			// Be sure remaining slots are cleared.
			this.RenderSlot( slot, null );	// [this.number_pages may have changed]
	}
	else {	// Fill the slots with the characters in a group.
		const is_enabled = character_groups[npage].is_enabled;

		for (var slot=1; slot <= 4; slot++)
			this.RenderSlot( slot, character_groups[npage].members[slot-1] );

		/* [The following statements were extracted from the slot loop because they
		   are not slot specific. Since the group selection screen has only one
		   button, I have added a new CharacterGroup property 'is_enabled'.] */

		$("#group-name-label").html( character_groups[npage].title );
		$("#group-button").html( is_enabled? "Select Group" : "Unavailable" );
		$("#group-button").attr( 'disabled', !is_enabled );
	}
}

// A character selection screen page is being changed. 
CharacterScreen.prototype.ChangePage = function( fast, step ) {
	const last_page = this.number_pages-1;		// [should be const!]
	var npage = this.page_num;

	if (!fast) {
		npage += step;			// right or left arrow by one

		if (npage > last_page) 	// wrap to the first page
			npage = 0;
		else if (npage < 0) 	// wrap to the last page
			npage = last_page;
	}
	else if (step == -1)		// fast backward to first page
		npage = 0;
	else if (step == 1)			// fast forward to last page
		npage = last_page;
//	else (step == 0)
//		clicked 'Go', which does nothing

	this.page_num = npage;
	this.SetPageNumber();		// give html the new page number [ex updatePage]
	this.RenderPage();			// give html the other new page data
}

// Bridges. [from <div id="/individual/group/-select-table", "main-select-area"]/
function changeIndividualPage(fast,step) { character_screen.single.ChangePage(fast,step); }
function changeGroupPage(fast,step) { character_screen.group.ChangePage(fast,step); }

/* For 'Back' on any character selection screen, return to the opponent selection screen.
   [from <button id='select-main-back-button'] */
function backToSelect( ) {
	character_screen.single.Hide();
	character_screen.group.Hide();
	opponent_screen.Show();
}

/* Change choice of information. kind: 1='Basic Info', 2='Credits', 3='More Info'
   [not slot specific. eliminate slot loop somehow?] */
CharacterScreen.prototype.ChangeInfo = function( kind ) {
	for (var k=1; k <= 3; k++)			// 3 values
		for (var slot=1; slot <= 4; slot++) {
			var $select = $(this.stats + slot + '-' + k);
			(k == kind)? $select.show() : $select.hide();
		}
}

/* Bridges for 'Basic Info',‘Credits’,'More Info' on a character selection screen.
   [from <div id=”individual-select-table”> class='..individual-credits-btn'] */
function changeIndividualStats( kind ) { character_screen.single.ChangeInfo( kind ); }
function changeGroupStats( kind ) { character_screen.group.ChangeInfo( kind ); }

//0x More bridges for 'Credits' on a character selection screen.
//$(".individual-credits-btn").on( 'click'.. calls character_screen.single.ChangeInfo(2)
//$(".group-credits-btn").on( 'click'.. calls character_screen.group.ChangeInfo(2)

// Toggle the visibility of the "table" on a character selection screen.
CharacterScreen.prototype.ToggleTable = function( ) {
	this.table_is_hidden? this.$table.show() : this.$table.hide();
	this.table_is_hidden = !this.table_is_hidden;
}

/* Bridges for Eye-Con. Rename these toggle/Single/Group/CharacterTable.
   [from <button class='..hide-table-button' glyphicon-eye-close or
   <button id='group-hide-button' glyphicon-eye-close] */
function hideSingleSelectionTable( ) { character_screen.single.ToggleTable(); }
function hideGroupSelectionTable( ) { character_screen.group.ToggleTable(); }

//0x The arrays $groupLabels and storedGroup are undeclared and unused. Also,
//function updateGroupScreen( player )		not found in index.html
//function updateRandomSelection			not found in index.html

/* For 'Select Group' on group selection screen, assign the members of the
   character group being displayed, replacing any existing assignments.
   [from <div id="group-select-table"> <button id="group-button"
   Absorbs groupScreenCallback. Compare with AssignFromSlot.] */

function selectGroup( ) { AssignGroup.call( character_screen.group ); }
function AssignGroup( ) {
	character_groups[ this.page_num ].Assign();
	ChangeScreen( this, opponent_screen );
}

/* For 'Select Opponent' on the single selection screen, assign the selected
   character to the player indexed by 'target'. See notes for SelectOpponent,
   which follows immediately. Compare with AssignGroup.
   [from <div id="individual-select-table"> <button id="select-slot-button-1:4"
   Absorbs individualScreenCallback. We know target is defined and that the
   slot isn't empty.] */

function selectIndividualOpponent( slot ) {
	AssignFromSlot.call( character_screen.single, slot ); }

function AssignFromSlot( slot ) {
	const pos = 4*this.page_num + (slot-1);
	const anum = AssignablesNumOf( pos ); 	// convert screen position to real index

	players[ this.target ].Assign( anum );
	this.target = undefined;
	ChangeScreen( this, opponent_screen );
}

/******* end Character Selection Screen Related Functions ******/
/****** begin Opponent Selection Screen Related Functions ******/

/* Either the 'Select Opponent' or the 'Remove Opponent' button on the
   opponent selection screen was clicked.

   When 'Select Opponent' is clicked on an opponent selection screen slot, its
   player number is saved as 'target'. The single character selection screen
   is then shown and one can navigate to a desired character. Once a character
   has been selected, AssignFromSlot assigns it to the target. The opponent
   selection screen is again displayed with the targeted player now visible.

   When 'Remove Opponent' is clicked, the selected player is deassigned.

   [from <div id="select-table"> <button id="select-slot-button-1:4".
   Old rebuilt (and reordered) assignables using UpdateSelectableOpponents,
   restoring all previously removed assignables and removing currently
   assigned ones. updateIndividualSelectScreen then displayed the new array.] */

function selectOpponentSlot(p) {SelectOpponent.call( character_screen.single, p );}
function SelectOpponent( p ) {
	if (!players[p].is_assigned) {			// Select Opponent was clicked
		this.target = p;					// remember which player was selected
		this.RenderPage();					// screen may have changed
		ChangeScreen( opponent_screen, this );
	}
	else // Remove Opponent. Deassign the player.
		players[p].Assign();
}

/* Assign an entire character group as players. This is the only case in which
   we may be overwriting existing assignments.
   [called by AssignGroup and clickedRandomGroup] */

CharacterGroup.prototype.Assign = function( ) {		// [more polymorphism]
	// It is safest to deassign all characters before making new assignments.
	for (var player of players)
		if (player.is_opponent && player.is_assigned)
			player.Assign();

	for (var player of players)
		if (player.is_opponent) {
			player.cnum = this.members[ player.num-1 ];	// must assign this way

			player.Assign( AnumOf(player.cnum) );
		}	// See Opponent.Assign notes for the case anum < 0.
}

/* If an anum is passed, assign the specified character to the player. Otherwise,
   deassign the player. Update the opponent screen.

   cnum points to the character being assigned and anum points to cnum. Remember
   this double dereference: 'character = inventory[ assignables[anum] ]'.
   cnum and anum are only meaningful if player.is_assigned.

   The approach of removing and restoring cnum's to prevemt duplication that is
   taken here is more complicated than it was before, but avoids constantly
   rebuilding, filtering, and sorting a long array.

   anum < 0 occurs when the character being assigned is not found in the
   assignables array owing to filtering. In this case this.cnum must be set
   externally. No reference can be made to the assignables array, but the player
   can be rendered and the buttons can be reset. */

// "To this player assign this character."
Opponent.prototype.Assign = function( anum ) {
	var n;
	this.is_assigned = (anum != undefined);

	if (this.is_assigned)
		this.anum = anum;

	if (this.anum < 0)			// this.anum is defined even when anum is not
		;
	// Remove the character from the assignables array.
	else if (this.is_assigned) {
		this.cnum = assignables[anum];
		for (n=0; n < assigneds.length && assigneds[n] < anum; n++);
		assigneds.splice( n, 0, anum );			// store original index
		assignables[ anum ] = null;
	}
	// Restore the character when the player is deassigned. (this.anum was
	// defined on assignment.)
	else {
		for (n=0; n < 4 && assigneds[n] < this.anum; n++);

		assigneds.splice( n, 1 );
		assignables[ this.anum ] = this.cnum;
	}

	this.Render();
	ResetButtons.call( opponent_screen );
}

/* [was updateSelectionVisuals. Compare to CharacterScreen.RenderSlot.
   Selector for advance-button not in index.html] */

Opponent.prototype.Render = function( ) {
	if (this.is_assigned) {
		var folder = inventory[this.cnum].folder;
		var xhr = $.get( folder + "behaviour.xml", $.noop, "text" );

		// Mini behavior load. Really only need dialogue.
		xhr.done( xml => {						// favor done over success
			var $start	= $(xml).find('start').find('state');	// [ex initPlayerState]
			this.label	= $(xml).find('label').text();
			this.folder = inventory[this.cnum].folder;	// cnum's are inventory indices
			this.$opp_label.html( this.label );
			this.$opp_dialogue.html( $start.html() );	// starting dialogue
			this.$opp_image.attr( 'src', folder + $start.attr('img') );
		});

		xhr.fail( (xhr, reason) => {			// favor fail over error
			alert( reason + " occurred while parsing the behavior file." ); });

		this.$opp_bubble.show();
		this.$opp_button.html( "Remove Opponent" );	// update the player's button
		this.$opp_button.removeClass( "smooth-button-green" );
		this.$opp_button.addClass( "smooth-button-red" );
	}
	else { // Deassign the player.
		this.$opp_bubble.hide();
		this.$opp_image.attr( 'src', BLANK_PLAYER_IMAGE );
		this.$opp_dialogue.html( "" );
		this.$opp_label.html( "Opponent " + this.num );
		this.$opp_button.html( "Select Opponent" );
		this.$opp_button.removeClass( "smooth-button-red" );
		this.$opp_button.addClass( "smooth-button-green" );
	}
/* [dead code: Show the Advance Dialogue button if cur_state is not the last.]
	var z = (this.current < this.state.length - 1)? 1 : 0;
	this.$opp_advance_button.css( {opacity: z} ); */
}

// Reset the opponent selection screen buttons. Non-slot specific.
function ResetButtons( ) {
	var num_assigned = -1;	// number of assigned players; -1 cancels human player.

	for (var player of players)				// count assigned players
		if (player.is_assigned) num_assigned++;

	// Turn on the game Start button when enough players have been assigned.
	this.$start_button.attr( 'disabled', (num_assigned < 2) );

	// Enable the three Random X buttons if and only if all players are assigned.
	for (var $button of this.$random_buttons)
		$button.attr( 'disabled', (num_assigned == 4) );

	// Turn off the Remove All button if no players are assigned.
	this.$remove_all_button.attr( 'disabled', (num_assigned == 0) );
}

/* For 'Eye-Con' on the opponent selection screen, toggle the visibility of the "table".
   [bridge. from <button class='bordered hide-table-button'] */
function hideSelectionTable( ) { ToggleOpponentTable.call(opponent_screen); }

function ToggleOpponentTable( ) {
	this.table_is_hidden? this.$table.show() : this.$table.hide();
	this.table_is_hidden = !this.table_is_hidden;
}

/*0x [For 'Advance Dialogue' on the opponent screen, an intended function.
   Advance to the next OpponentState and refresh the opponent screen. 
   Compare with Opponent.UpdateVisuals] [from unknown] */
function advanceCurrentDialogue( p ) { players(p).AdvanceDialogue(); }

Opponent.prototype.AdvanceDialogue = function() {
	var cur_state;
	var direction;

	this.current++;					// [needs a guard]
	cur_state = this.state[this.current];

	if (cur_state.direction != null)			// direct the dialogue bubble
		direction = "dialogue-bubble dialogue-" + cur_state.direction;
	else
		direction = "dialogue-bubble dialogue-centre";

	this.$opp_button.removeClass();
	this.$opp_button.addClass( direction );
	// [dev: in original $opp_button was not altered past here]
	this.Render();
}

/******
 These are callbacks from the 'main-select-area' of the opponent selection
 screen. They are presented in order of button appearance from left to right.
 [<div id='select-table'> <div id='main-select-area']
******/

/* For 'Back' on opponent selection screen, return to the title screen.
   [from <button id=select-main-back-button] */
function backSelectScreen( ) {
	LoadTitleScreen();
	ChangeScreen( opponent_screen, title_screen );
}

/* For 'Select Group' on opponent selection screen, change to the group
   selection screen. [from button id=select-group-button] */
function clickedSelectGroupButton( ) {
	character_screen.group.RenderPage();
	ChangeScreen( opponent_screen, character_screen.group );
}

/* For 'Random Group' on opponent selection screen, pick a random character
   group and assign its members as opponents.
   [from <button id=select-random-group-button] */

function clickedRandomGroupButton( ) {
	var npage = RandomInteger( 0, character_groups.length );

	character_groups[npage].Assign();
} // opponent screen is showing

/* For 'Start' on opponent selection screen, change to the game screen.
   [from <button id=main-select-button] */

function advanceSelectScreen( ) {
	opponent_screen.Hide();				// [absorbs advanceToNextScreen]
	LoadGameScreen();					// [use ChangeScreen?]
	$game_screen.show();
}

/* For 'Random Males', 'Random Females', and 'Random Fill' on opponent selection
   screen, assign to each unassigned player a random character, optionally of a
   preferred gender. Start with the assignables list since it contains only
   unassigned characters.
   [A lot of programming gibberish in this one, but it can't be helped.
   from <button id=select-random-male-button, select-random-female-button, and
   select-random-button] */

function clickedRandomFillButton( has_preferred_gender ) {	// arg is a predicate
	const any = (has_preferred_gender == undefined);

	// Filter out removed cnum's and those that fail the gender specification.
	var a_map = assignables.map( (cnum,anum) => {
		var pass = (cnum != null && (any || has_preferred_gender( inventory[cnum] )));
		return pass? anum : null;			// we must return something
	});
	a_map = a_map.filter( anum => {return anum != null} ); 	// discard failures

	/* For each unassigned player pick a random a_map member and pass it to Assign().
	   Then remove the assigned character from both lists. */
	for (player of players)
		if (!player.is_assigned && a_map.length > 0) {	// human is always assigned
			var r = RandomInteger( 0, a_map.length );

			player.Assign( a_map[r] );		// slot is empty; player unassigned
			a_map.splice( r,1 );			// remove the used index
			// The assignable itself will be removed by Assign().
		}
} // opponent screen is showing

/* For 'Remove All' on opponent selection screen, deassign all opponents.
   [from <button id=select-remove-all-button] */

function clickedRemoveAllButton( ) {
	for (var player of players)
		if (player.is_opponent && player.is_assigned)
			player.Assign();		// this deassigns the player
} // opponent screen is showing

/****** End Opponent Selection Screen Related Functions ******/

/* Word-wrapping function. Insert a fixed-size HTML element with the specified text.
   Allows the content to be either word-wrapped if the text is long and spaces are
   present, or word-broken if the text is long and no spaces are present. */

function wordWrapHtml( text ) {
	text = text || "&nbsp;";
	return "<table class=\"wrap-text\"><tr><td>" + text + "</td></tr></table>";
}

/****** Dynamic Dialogue and Image Counting Functions ******/

/* Originally, updateOpponentCountStats counted the number of unique dialogue
   lines and the number of pose images for the characters visible on a given
   character screen page and displayed them. But these Character properties
   really only need to be calculated once. Consequently, we have divided the
   function into a 'show' part and a 'count' part, the former being absorbed by
   CharacterScreen.RenderSlot and the latter being here. */

/* Count the number of unique lines of dialogue and the number of images in a
   character's behavior file. */
Character.prototype.GetCounts = function( ) {
	var xhr = $.get( this.folder + "behaviour.xml", $.noop, "text" );

	xhr.fail( (xhr,reason) => {			// same as error:
		alert( reason + " occurred while parsing " + this.folder + "behavior.xml" );
	});

	xhr.done( xml => {					// must use arrow function. same as success:
		var lines = [];
		var poses = [];

		$(xml).find('state').each(		// extract dialogue and images
			function( n, st ) {
				lines[n] = st.textContent.trim();
				poses[n] = st.getAttribute( "img" );
			}
		);
		// total_lines = lines.length;

		/* Count lines of dialogue with identical lines counting as one. The
		   filter counts only the last occurrence of elements in the array. */

		lines = lines.filter( (line,n) => {return n == lines.lastIndexOf(line);} );
		this.unique_lines = lines.length;

		/* Count images used in dialogues with identical images counting as one.
		   Note that this number will differ from the actual number of images if an
		   image is never used or if the dialogue refers to non-existent images. */

		poses = poses.filter( (pose,n) => {return n == poses.lastIndexOf(pose);} );
		this.num_images = poses.length;

		if (DEBUG)
			console.log( "GetCounts for "  + this.folder + ", " + this.unique_lines +
				" lines, " + this.num_images + " images" );
	});
}

//0x update/Individual/Group/CountStats		obsolete bridges

/****** Search Modal Functions ******/

/* [from <div id="individual-select-screen" "individual-select-table"
   "main-select-area" "select-page-selection" <button class=".. go-page-button"] */
function openSearchModal( ) {
	$('#search-modal').modal( 'show' );
}

/* For 'Search' on single selection screen, totally rebuild the assignable
   characters array, applying filtering and sorting options. Since the array is
   being completely rebuilt from inventory, we don't need to restore any cnum's
   that were removed from it. Assigned players are re-indexed after sorting.
   [Initial values of name and source are "", not null.
   Absorbs updateSelectableOpponents]

   [from <div id="search-modal" <button..data-dismiss="modal"] */
function closeSearchModal( ) {
	var name	= $search_name.val().toLowerCase();
	var source	= $search_source.val().toLowerCase();

	assignables = [];				// reset

	for (var char of inventory) {	// search for matches
		var pass = (					// filter by name
			name == "" ||
			char.label.toLowerCase().includes( name ) ||
			char.first.toLowerCase().includes( name ) ||
			 char.last.toLowerCase().includes( name ));

		pass = pass && (			// filter by source
			source == "" ||
			char.source.toLowerCase().includes( source ) );

	    // No can filter by tags. Tags are a Player, not a Character, property.

	    /* The gender filter failure condition:
		fail  = (chosen_gender == 2 && char.gender !== eGender.male) ||
				(chosen_gender == 3 && char.gender !== eGender.female)
		is correct. What's less obvious is its logical complement, namely: */

		pass = pass && (
			(chosen_gender == 1) ||
			(chosen_gender == 2 && char.gender == eGender.male) ||
			(chosen_gender == 3 && char.gender == eGender.female) );

		if (pass)
			assignables.push( char.num ); 		// inventory array is unaffected
	}

	/* "If a unique match was made, clear the search..". See
	   character_screen.single.RenderPage for this case.

	   Assignables now has featured order. If a custom sorting option has been
	   chosen, apply it. [Argument is now an array.] */

	if (sort_by.hasOwnProperty( sort_option ))
		SortAssignables( sort_by[sort_option] );

	/* Re-index the assigned characters. See Opponent.Assign for an explanation
	   of what anum < 0 means. */
	for (player of players)
		if (player.is_opponent && player.is_assigned)
			player.Assign( AnumOf(player.cnum) );

	// assignables.length may have changed.
	character_screen.single.page_num = 0;
	character_screen.single.number_pages = Math.ceil( assignables.length/4 );
	character_screen.single.SetPageNumber();
	character_screen.single.SetMaximumPage();
	character_screen.single.RenderPage();
}

/* For 'Clear' on search modal, clear the name and source fields (only).
   [from <div id="search-modal" <button..data-dismiss="modal"] */
function clearSearch( ) {
	$search_name.val( "" );				// [$search_tag removed]
	$search_source.val( "" );
	closeSearchModal();
}

/* Set chosen gender. arg: 1=All, 2=Male, 3=Female.
   [From <div id="search-modal, id="search-gender-1:3] */
function changeSearchGender( gender ) {
	chosen_gender = gender;
	setActiveOption( $search_gender_options, gender );
}

/****** Sorting Functions ******/

/* Register event handler for the sort options dropdown menu. Fires when the user
   clicks on a dropdown item. */
$(".sort-dropdown-options li").on( "click",
	function(e) {
		sort_option = $(this).find('a').html();
		// Set the dropdown text to the current sort mode.
		$("#sort-dropdown-selection").html( sort_option );	// default Featured
	}
);

/* Sort assignable characters according to one or more fields. Prefixing a field
   with a hyphen will cause it to be sorted in descending order.
   Examples:
		SortAssignables( "first" );	// arr will be sorted by first names (A-Z)
		SortAssignables( "-last" );	// arr will be sorted by last names (Z-A)
		SortAssignables( "layers", "-first" );
   In the last example characters will be sorted by number of layers (1-8) and
   next, for those with the same number of layers, by first name in order (Z-A). */

function SortAssignables( fields ) {	// argument is an array of sort fields
	function Compare( cnum1, cnum2 ) {
		var compare;
		var order;
		var val1, val2;

		// If both elements have the same value for a field, compare the next one.
		for (var field of fields) {
			order = (field[0] == "-")? -1 : 1;
			if (order == -1)
				field = field.substr(1);

			val1 = inventory[cnum1][ field ];
			val2 = inventory[cnum2][ field ];
			if (val1 != val2) break;
		}

		if (val1 < val2)
			compare = -1;
		else if (val1 > val2)
			compare = 1;
		else
			compare = 0;
		return order*compare;	 	// sort direction
	}

	assignables.sort( (cnum1,cnum2) => Compare(cnum1,cnum2) );
}
