Release Notes.2 for
save.js
spniTitle.js
spniOption.js
spniGallery.js
spniEpilogue.js

To view these files click to open them and then press the EDIT button. (It appears on the same line as the name of the file.) The files will then appear formatted as they should be and the comments will be much easier to read. Please choose 'Cancel' when you are done browsing as these files are open to the public. Also, remember that the meta-comments enclosed by [] brackets are temporary and will ultimately be removed.

Release Notes.1, which you should read, contains some useful overall information about this project. Also read the file, "What-is-RISA?", to get an understanding of the motivation and the methodology that applies here.

save.js

The function LoadSavedGame does not work properly. When it is called for the first time after starting the program, Cookie.Get returns an undefined value for the cookie that should contain the previously saved game state. The default game_state object is loaded instead. save.js passes unit testing in all other respects.

Advancing from the title screen to the opponent screen causes the current game_state to be saved. Clicking on the opponent screen 'Back' button then returns the player to the title screen and correctly loads the previously saved game_state. That is, the variable cookie is now properly defined. However, exiting and restarting the game causes the cookie to become undefined once again. More research must be done to understand why this happens.

spniTitle.js
spniOption.js

There isn't much to say here, but these two files afford excellent examples of the kind of code reduction that I expect when I recover a program. If you are a programmer, compare these functions to their counterparts in the original source.

The function setTableStyle is a particularly good example of simplification. It is admittedly an unimplemented game feature, but the current version is nevertheless equivalent to the original version. If you were going to implement this function, from which version would you prefer to start?

spniGallery.js
spniEpilogue.js

What is noteworthy here is the greatly increased integration of the two files. The class Epilogue is now common to both files as are the getter function GetEpiloguesFrom and the functions Epilogue.Start and Epilogue.Play. Merging like components from the two files requires the introduction of a small additional amount of code which is not needed if the components are left separate. However, having common code for both files results in an overall reduction of complexity as well as of size.
