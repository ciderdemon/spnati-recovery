class EpilogueScreen extends Screen {
	constructor( ) {
		super( epilogue_screen_sel );
	}
}

// .find returns undefined when no matches exist.
class TextBox {
	constructor( n, txt ) {
		this.num	= n;
		this.x		= $(txt).find("x").html().trim();
		this.y		= $(txt).find("y").html().trim();
		this.text	= $(txt).find( "content" ).html().trim();
		var w		= $(txt).find("width").html();
		var a		= $(txt).find("arrow").html();

		if (this.x.toLowerCase() == "centered")	// centering was requested
			this.x = new CenteredPosition( this.width );

		this.width = (w !== undefined && w.length > 0)? w.trim() : "20%";
		// optional. defaults to 20%.

		this.arrow = (a !== undefined && a.length > 0)?
			("arrow-" + a.trim().toLowerCase()) : '';
		// "arrow-.." is HTML class name for dialogue bubble arrows. optional.
		}
}

// Construct all the textboxes.
class EndingScreen {
	constructor( screen, folder ) {
		this.image		= folder + $(screen).attr( "img" ).trim();
		this.textboxes	= [];

		$(screen).find('text').each( (n,txt) => {
			this.textboxes[n] = new TextBox( n, txt );
		});
	}
}

class Epilogue {
	constructor( n , char, $epilogue ) {
		this.num		= n;				// epilogue number
		this.owner		= char.num;			// number of the character who owns the epilogue
		this.label		= char.label;		// owner's label
		this.folder		= char.folder;		// owner's folder
		this.gender		= $epilogue.attr('gender');			// not used in this file
		this.title		= $epilogue.find("title").html().trim();	// [epilogueTitle]
		this.screens	= [];

		$epilogue.find( "screen" ).each( (n,screen) => {
			this.screens[n] = new EndingScreen( screen, this.folder );
		});
		this.last_screen = this.screens.length - 1;
		this.nscreen	= 0;	// current screen num [was epilogueScreen (no $)]
		this.ntxtbox	= 0;	// textbox num for current screen [was epilogueTextCount]
	}
}

// Epilogue selection modal related elements
const $list				= $('#epilogue-list'); 			//2x HTML <li elements for epilogues
const $accept_button	= $('#epilogue-modal-accept-button');	//3x calls Epilogue.Initialize
const $spilogue_modal	= $('#epilogue-modal');			//2x

// Epilogue screen related elements
const epilogue_screen_sel = '#epilogue-screen';		//2x selector only [in Textbox.Draw &
													// screen constructor]
const $epilogue_screen	= $(epilogue_screen_sel); 	// in Start, Play. [was $epilogueScreen]
													// or = epilogue_screen.$display

var epilogue_screen	= new EpilogueScreen();
var epilogues 		= [];			// array of epilogues
var open_textboxes	= [];			// textboxes currently open for the epilogue playing
var chosen_ep		= -1;			// number of epilogue to play

/* The numeric part of a string, e.g. "20%" -> 20. Apparently, one doesn't need to
   remove the % (or anything else) from the string before doing the conversion. */
function NumericPart( str ) {
    return parseFloat(str);
}

// The left setting that will center a textbox of specified width. Assumes a % width.
function CenteredPosition( width ) {
	const w = NumericPart(width);	// numeric value of the width
	const left = 50 - (w/2);		// textbox center is now at the 50% position
	return left + "%";
}

// [I hate this unpredictable asynchronous crap.]
function GetEpiloguesFrom( char, n ) {
	var xhr = $.ajax( char.folder + 'behaviour.xml',
		{async: false, method: 'GET', dataType: "text"} );

	xhr.fail( (xhr, reason) => {
		alert( reason + " occurred loading epilogues from " + char.first );
	});

	xhr.done( xml => {							// favor .done over success:
		$(xml).find('epilogue').each( (unused,epilogue) => {
			epilogues[n] = new Epilogue( n, char, $(epilogue) );
			n++;
		});
	});
	return n;
}

/* Load all epilogues from all opponents and make line items for them.
   ["Get the XML tree related to epilogues for the human player's gender.
   var epi = $( $.parseXML(xml) ).find( 'epilogue[gender="'+human_gender+'"]' );
   Use parseXML() so <image> tags come through properly. (IE doesn't like this.)"
   Absorbs addEpilogueEntry] */

function InstallEpilogues( ) {						// [was loadEpilogueData]
	var n = 0;

	for (let player of players)
		if (player.is_opponent && player.is_assigned) {
			var char = inventory[player.cnum];
			if (char.has_ending)
				n = GetEpiloguesFrom( char, n );
		}

	// Make an HTML line item for each epilogue so it can be selected.
	for (let ep of epilogues) {
		const first = inventory[ep.owner].first;
		const last	= inventoty[ep.owner].last;
		const name = (first !== '' && last !== '')? concat( first, ' ', last ) :
			concat( first, last );					// [use owner label?]
		const text = name + ': ' + ep.title;

		$list.append(
			'<li id="epilogue-option-'+ep.num+'" class="epilogue-entry"> ' +
			'<button onclick="SelectEpilogue('+ep.num+')">'+text+'</button> </li><br>' );
	}
}

// Draw an epilogue textbox from the current screen.
TextBox.prototype.Draw = function( ) {
	// Let the writer use ~name~ to refer to the human player.
	var text = this.text.replace( '~name~', players[HUMAN].label );

	// Create an HTML element of type 'div' and set attribute 'id' to given value.
	var element = document.createElement('div');
	var box_div = $(element).attr( 'id', "#epilogueDivBox" + this.num );
	var dialog_n = 'epilogue-dialogue-' + this.num;

	// Copy textbox data to box_div.
	var span = '<span id="'+dialog_n+'" class="dialogue-bubble">'+text+'</span>';
	var div2 = '<div class="dialogue-area"> ' + span + ' </div>';

	box_div.after().html(
		'<div id="epilogue-bubble-'+this.num+'" class="bordered dialogue-bubble-area modal-dialogue">'
			+ div2 + '</div>' );

	// Use css to position the textbox.
	box_div.css( 'position', 'absolute' );
	box_div.css( 'left',	this.x );
	box_div.css( 'top',		this.y );
	box_div.css( 'width',	this.width );

	// Attach the new div element to the epilogue screen.
	box_div.appendTo( epilogue_screen_sel );
	$('#'+dialog_n).addClass( this.arrow );	// [insert in span]

	// Keep track of the open textboxes, so they can be removed later.
	open_textboxes.push( box_div );
};

// Close all open textboxes.
function CloseTextboxes( ) {
	while (open_textboxes.length > 0) {
		var box_div = open_textboxes.pop();
		box_div.remove();
	}
}

/* An epilogue selection button, constructed by class Epilogue, was clicked.
   [from '<li id="epilogue-option-n" <'<button onclick="selectEpilogue(n)'] */
function SelectEpilogue( n ) {
	chosen_ep = n;

	// Make sure no other epilogue is active.
	for (let ep of epilogues)
		$('#epilogue-option-' + ep.num).removeClass("active"); // created by new Epilogue

	$('#epilogue-option-' + chosen_ep).addClass("active");	// mark epilogue as chosen
	$accept_button.prop( "disabled", false );		// let the player accept the epilogue
}

// Show the epilogue selection modal. It lets you choose an epilogue or restart the game.
// [not in index.html; from where?]
function ShowEpilogueModal( ) {
	const $header = $('#epilogue-header-text');
	var header;
	var human_won;				// did the human player win?

	// Clear the epilogue selection modal.
	$header.html('');
	$list.html(''); 			// remove any epilogue selection iine items
	epilogues = [];
	chosen_ep = -1;				// reset any currently-chosen epilogue

	$accept_button.prop( "disabled", true );
	// Don't let the player accept an epilogue until they've chosen one.

	for (let player of players)
		if (!player.out) {
			human_won = (player.num == HUMAN);
			break;
		}

	// The epilogues are only for when the player wins.
	if (human_won)
		InstallEpilogues();			// load epilogues and line items

	// If any epilogues are available, enable the 'Accept' button.
	var have_epilogues = (epilogues.length > 0);
	$accept_button.css( "visibility", have_epilogues? "visible" : "hidden" );

	// Display header text.
	if (!human_won)
		header = "Well you lost. And you didn't manage to make any new friends. But you saw some people strip down and show off, and isn't that what life is all about?<br>(You may only view an ending when you win.)";		// lost:

	else if (have_epilogues)
		header = "You've won the game, and possibly made some friends. Who among these players did you become close to?"; 		// won:

	else
		header = "You've won the game, and possibly made some friends? Unfortunately, none of your competitors are ready for a friend like you.<br>(None of the characters you played with have an ending written.)";			// won_but:
	// Currently there is no support for endings when another player wins.

	$header.html( header );
	$spilogue_modal.modal( "show" );			// show epilogue selection modal
}

/* Initialize the epilogue.
   [bridge. from <div id="epilogue-modal" <button id="epilogue-modal-accept-button"] */
function doEpilogue( ) { epilogues[chosen_ep].Start( true ); }

Epilogue.prototype.Start = function( from_game ) {		// else call is from gallery
	if (from_game)
		UnlockEnding( this.folder, this.title );		// [else coming from Gallery.js]

	this.nscreen = 0;			// Set epilogue position to start.
	this.ntxtbox = 0;			// [dev: undeclared var epilogueText prob meant to be this]
	CloseTextboxes();			// just in case, close any open textboxes [need?]

	// In the at start (0,0) position. [epilogue.Play(0) no longer works.]
	this.screens[0].textboxes[0].Draw();
	$epilogue_screen.css( 'background-image', 'url("' + this.screens[0].image + '")' );
	SetButtons( false, true );

	// "currently coming from title screen, because this is for testing"
	epilogue_screen.Show();
	from_game? title_screen.Hide() : gallery_screen.Hide();
	if (from_game)
		$spilogue_modal.modal( "hide" );
}

/* Move the epilogue forwards or backwards by one step. Only values for step are +1
   to go to the next line, -1 to go to the previous line.
   [from <div id="epilogue-screen" <button..id="epilogue-/next/prev/-button"
		or <button.. .. -on-image"
   Should be function PlayEpilogue( step )] */

function progressEpilogue( step ) { epilogues[chosen_ep].Play(step); }

Epilogue.prototype.Play = function( step ) {
	const forward		= (step==1);
	var   screen		= this.screens[this.nscreen];
	const last_txtbox	= screen.textboxes.length - 1;
	const screen_limit	= forward? this.last_screen : 0;
	const txtbox_limit	= forward? last_txtbox : 0;

	// ['!=' = forward? '<' : '>'. There is an unfolded version of this in junk.js]

	// Step forwards or backwards, possibly to another screen. [Is this a fold too far?]
	if (this.ntxtbox != txtbox_limit) {
		this.ntxtbox += step;

		if (forward)
			screen.textboxes[this.ntxtbox].Draw();	// draw textbox
		else {
			let box_div = open_textboxes.pop();
			box_div.remove();					// remove the last drawn textbox [.Draw(-1)]
		}
	}
	else if (this.nscreen != screen_limit) { 	// go to first/last textbox of next/prev screen
		CloseTextboxes();							// takes out the last box if !forward
		this.nscreen += step;
		screen = this.screens[this.nscreen];
		$epilogue_screen.css( 'background-image', 'url("' + screen.image + '")' );
		this.ntxtbox = forward? 0 : last_txtbox;	// reverse of txtbox_limit

		if (forward)
			screen.textboxes[0].Draw();				// 0 = this.ntxtbox
		else
			for (let box of screen.textboxes)		// on backwards screen change,
				box.Draw();							// redraw all boxes 
	} // else we are at end or at start
	// open_textboxes.length == this.ntxtbox+1 is invariant

	SetButtons( forward, (this.nscreen == screen_limit && this.ntxtbox == txtbox_limit) );
}

// Set the previous, next, and restart buttons properly.
function SetButtons( forward, at_limit ) {
	const $reset_button		= $('#epilogue-restart-button');
	const at_start	= !forward && at_limit;
	const at_end	= forward && at_limit;

	// At the first position. disable the 'previous' button.
	$('#epilogue-prev-button').prop(	"disabled", at_start );
	$('#epilogue-prev-button').css(		"opacity",  at_start? 0.4 : 1.0 );
	$('#epilogue-prev-on-image').prop(	"disabled", at_start );

	// At last position. disable the 'next' button and enable the 'restart' button.
	$('#epilogue-next-button').prop(	"disabled", at_end );
	$('#epilogue-next-button').css(		"opacity",  at_end? 0.4 : 1.0 );
	$('#epilogue-next-on-image').prop(	"disabled", at_end );

	$reset_button.prop( "disabled", !at_end );
	$reset_button.css( "visibility", at_end? "visible" : "hidden" );
}

