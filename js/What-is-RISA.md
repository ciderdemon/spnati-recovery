The program files you see here are not simply rewritten versions of the original files in any conventional sense of the word "rewritten". They are rather the products of a ground-breaking methodology known as Reconstructive Inferential Software Analysis or RISA.

What is Inferential Software Analysis?

Inferential Software Analysis is a technique by which reductive, function preserving transformations are iteratively applied to a program in order to simplify it so that it becomes comprehensible to the solver -- at least to the point where the next iteration can safely be carried out. The essential characteristic of inferential methods is that they scrupulously preserve the functionality of the underlying document. 

In this respect Inferential Software Analysis resembles the familiar process of solving a mathematical equation. In either case the solution process involves simplifying the subject until its meaning is ultimately revealed. Operationally, ISA allows the solver to answer questions about the subject, which in the end come down to, "What does this do?". That is wny ISA should be regarded as an analytic methodology rather than as a synthetic one.

Why use this?

ISA transformations reconstruct the subject to which they are applied, be it a single program or an entire project, improving its clarity, reducing its complexity, and generally increasing the elegance of its expression. This in turn vastly increases the accuracy, reliability, maintainability, and modifyabilty of the subject.

When the decision is then made to permanently replace the original version of the subject with its reconstructed version, the subject is said to have been recovered. As a metaphor for recovery imagine an airplane crashed in a jungle, with its parts and pieces scattered everywhere, concealed by the jungle canopy and almost unrecognizable as an aircraft. The recovery effort painstakingly rebuilds the airplane, thereby revealing its identity. In the case of software, recovery reveals the "true solution" to the problem that the subject wsa meant to solve.

The typical volumetric reduction of a recovered subject is around 50%. (If I can't get that much, I'm having a Bad Code Day.) It is not uncommon for this figure to rise above 60%. The figure is approximately constant whether lines are counted, all characters, or just non-comment characters. The reduction in complexity, which is what really matters, is more difficult to measure, but browsing through recovered documents and comparing them with the original versions suggests to me an order of magnitude yield. This is the stuff of industrial revolutions.

How are outcomes like this even possible?

This is a question with a potentially long answer, so let us just say that several factors are involved:
•	incomplete knowledge of the language in use
•	general lack of writing skills
•	lack of time in which to complete the work
•	total management ignorance of the actual state of the practice

I am constantly amazed by the amount of work that remains to be done on the typical program or the number of errors it contains. This is not really surprising, as programmers are usually hired according to their familiariy with a stated application domain, rather than according to their programming skills. If you don't believe this, read some of the job openings posted on the net and what employers regard as their job requirements. The sbility to program well is not on the list. After all, my dog can write programs.

Documentation

Analytic annotations are notes that a program analyst writes to himself. By convention they are enclosed in square brackets. The notation [was ..], for example, recalls the provenance of elements that have been renamed or absorbed by new code. The notation [dev: ..] on the other hand, notes a "deviation" from the precise behavior of the original program. Often it appears where an error has been corrected.

These working notes might be compared to the scaffolding on a construction site in that they will (mostly) be removed when the project is over. Because these notes relate to the ongoing work rather than to the subject as such, they can be termed "meta-comments".

In addition it is helpful to prepare a derivation document, which shows as clearly as possible where the functionality of old files is incorporated at lease partially in the new files.
